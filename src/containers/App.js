import React, { PureComponent } from 'react';

import Aux from '../hoc/Aux';
import WithClass from '../hoc/WithClass';
// import withClass from '../hoc/WithClass-old';

import Cockpit from '../components/Cockpit/Cockpit';
import Persons from '../components/Persons/Persons';

import './App.css';

class App extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            persons: [
                { id: "001", name: "Max", age: 28 },
                { id: "002", name: "Manu", age: 29 },
                { id: "003", name: "Mary", age: 26 }
            ],
            showPersons: false,
            toggleClicked: 0
        };

        console.log("[App.js] inside constructor");
    }

    componentWillMount() {
        console.log("[App.js] inside componentWillMount()");
    }

    componentDidMount() {
        console.log("[App.js] inside componentDidMount()");
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log("[UPDATE App.js] inside shouldComponentUpdate()", nextState);
    //     return nextState.persons !== this.state.persons ||
    //            nextState.showPersons !== this.state.showPersons;               
    //     // return true;
    // }

    componentWillUpdate(nextProps, nextState) {
        console.log("[UPDATE App.js] inside componentWillUpdate", nextState);
    }

    componentDidUpdate() {
        console.log("[UPDATE App.js] inside componentDidUpdate()");
    }

	togglePersons = () => {
        this.setState((prevState) => 
            { return { showPersons: !prevState.showPersons, 
                       toggleClicked: prevState.toggleClicked + 1 }
        });
	}

	removeName = (idx) => {
		let persons = [...this.state.persons];
		
		persons.splice(idx, 1);

		this.setState({ persons: persons });
	}

	changeName = (e, id) => {
		let idx = this.state.persons.findIndex(item => item.id === id);
		let persons = [...this.state.persons];

		persons[idx]["name"] = e.target.value;
		
		this.setState({ persons: persons });
	}

    showPersons = () => {
        if (!this.state.showPersons) { return null };
        
        return (
            <Persons showPersons={this.state.showPersons}
                     persons={this.state.persons}
                     onClick={this.removeName}
                     onChange={this.changeName} />
        );
    }

	setClass = () => {
		let arrLength = this.state.persons.length;
		let classes = [];

		if (arrLength <= 2) { classes.push("red") };
		if (arrLength <= 1) { classes.push("bold") };

		return classes.join(" ");
	}

	render() {
        console.log("[App.js] inside render()");

		return (
			<Aux>
                <button onClick={() => this.setState({ showPersons: true })}>Show Persons</button>

                <Cockpit persons={this.state.persons}
                         showPersons={this.state.showPersons}
                         togglePersons={this.togglePersons} />

                {this.showPersons()}
            </Aux>
		);
	}
}

export default WithClass(App, "App");
