import React, { PureComponent } from 'react';

import Person from './Person/Person';

class Persons extends PureComponent {
    constructor(props) {
        super(props);

        this.lastPersonRef = React.createRef();

        console.log("[Persons.js] inside Constructor");
    }

    componentWillMount() {
        console.log("[Persons.js] inside componentWillMount()");
    }

    componentDidMount() {
        console.log("[Persons.js] inside componentDidMount()");
        this.lastPersonRef.current.focus();
    }

    componentWillReceiveProps(nextProps) {
        console.log("[UPDATE Persons.js] inside componentWillReceiveProps()", nextProps);
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log("[UPDATE Persons.js] inside shouldComponentUpdate()", nextProps, nextState);
    //     return nextProps.persons !== this.props.persons ||
    //            nextProps.onClick !== this.props.onClick ||
    //            nextProps.onChange !== this.props.onChange;
    //     // return true;
    // }

    componentWillUpdate(nextProps, nextState) {
        console.log("[UPDATE Persons.js] inside componentWillUpdate", nextProps, nextState);
    }

    componentDidUpdate() {
        console.log("[UPDATE Persons.js] inside componentDidUpdate()");
    }

    render() {
        console.log("[Persons.js] inside render()");

        return (
            this.props.persons.map((item, idx) => {
                return (
                    <Person key={item.id} 
                            name={item.name}
                            age={item.age}
                            position={idx}
                            ref={this.lastPersonRef}
                            onClick={() => this.props.onClick(idx)}
                            onChange={(e) => this.props.onChange(e, item.id)} />
                );
            })
        );
    }
};

export default Persons;