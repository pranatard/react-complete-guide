import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Aux from '../../../hoc/Aux';
import WithClass from '../../../hoc/WithClass';
// import withClass from '../../../hoc/WithClass-old';

import './Person.css';

class Person extends Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef();

        console.log("[Person.js] inside Constructor");
    }

    componentWillMount() {
        console.log("[Person.js] inside componentWillMount()");
    }

    componentDidMount() {
        console.log("[Person.js] inside componentDidMount()");
    }

    focus () {
        this.inputRef.current.focus();
    }

    render() {
        console.log("[Person.js] inside render()");

        return (
            <Aux>
                <p onClick={this.props.onClick}>I'm {this.props.name} and I'm {this.props.age} years old</p>
                <input type="text" onChange={this.props.onChange} 
                        value={this.props.name} ref={this.inputRef} />
            </Aux>
        );
    }
};

Person.propTypes = {
    onClick: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    onChange: PropTypes.func
};

export default WithClass(Person, "Person");