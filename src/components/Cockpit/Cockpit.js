import React from 'react';

import Aux from '../../hoc/Aux';

import './Cockpit.css';

const cockpit = (props) => {
    let arrLength = props.persons.length;
    let classes = [];

    if (arrLength <= 2) { classes.push("red") };
    if (arrLength <= 1) { classes.push("bold") };
        
    return (
        <Aux>
            <h1>I'm a React App</h1>
            <p className={classes.join(" ")}>This is working!</p>

            <button className={"btnToggle " + (props.showPersons ? "" : "active")} onClick={props.togglePersons}>Toggle Persons</button>
        </Aux>
    );
};

export default cockpit;